package message

import (
	"fmt"
	"io"
)

type Writer struct {
	writer io.Writer
}

func NewWriter(writer io.Writer) *Writer {
	return &Writer{
		writer: writer,
	}
}

func (w *Writer) Write(message interface{}) error {
	switch m:=message.(type) {
	case ClientMessage:
		return w.writeString(fmt.Sprintf("%s%s\n", SENT, m.Message))
	case ReceivedMessage:
		return w.writeString(fmt.Sprintf("%s%s:%s\n", RECEIVED, m.ClientName, m.Message))
	case NameMessage:
		return w.writeString(fmt.Sprintf("%s%s\n", NAME, m.Name))
	}
	return nil
}

func (w *Writer) writeString(message string) error {
	_, err := w.writer.Write([]byte(message))
	return err
}
