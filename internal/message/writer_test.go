package message

import (
	"bytes"
	"testing"
)

func TestWriter_Write(t *testing.T) {
	tests := []struct {
		messages []interface{}
		result   string
	}{
		{
			messages: []interface{}{
				ReceivedMessage{
					Message:    "PWET",
					ClientName: "Ghislain",
				},
			},
			result: "[RECEIVED]Ghislain:PWET\n",
		},
	}

	buffer := new(bytes.Buffer)

	for _, test := range tests {
		buffer.Reset()
		writer := NewWriter(buffer)
		for _, message := range test.messages {
			if writer.Write(message) != nil { t.Errorf("Error when writing Message: %v", message) }
		}
		t.Log(buffer.String())
		if buffer.String() != test.result { t.Errorf("Expected %s, Actual: %s", test.result, buffer.String()) }
	}
}
