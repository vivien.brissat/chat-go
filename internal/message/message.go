package message

const (
	SENT = "[SENT]"
	RECEIVED = "[RECEIVED]"
	NAME = "[NAME]"
)

type ClientMessage struct {
	Message string
}

type ReceivedMessage struct {
	Message    string
	ClientName string
}

type NameMessage struct {
	Name string
}

