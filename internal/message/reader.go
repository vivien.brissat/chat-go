package message

import (
	"bufio"
	"errors"
	"io"
	"log"
	"strings"
)

var UnknownMessageError = errors.New("unknown Message")

type Reader struct {
	reader *bufio.Reader
}

func NewReader(reader io.Reader) *Reader  {
	return &Reader{
		reader: bufio.NewReader(reader),
	}
}

func (r *Reader) Read() (message interface{}, err error) {
	messageType, err := r.reader.ReadString(']')
	if err != nil {
		return nil, err
	}
	switch messageType {
	case SENT:
		messageContent, err := r.reader.ReadString('\n')
		if err != nil {
			return nil, err
		}
		return ClientMessage{Message: strings.TrimSuffix(messageContent, "\n")}, nil
	case RECEIVED:
		messageAuthor, err := r.reader.ReadString(':')
		if err != nil {
			return nil, err
		}
		messageContent, err := r.reader.ReadString('\n')
		if err != nil {
			return nil, err
		}
		return ReceivedMessage{Message: strings.TrimSuffix(messageContent, "\n"), ClientName: strings.TrimSuffix(messageAuthor, ":")}, nil
	case NAME:
		messageAuthor, err := r.reader.ReadString('\n')
		if err != nil {
			return nil, err
		}
		return NameMessage{Name: strings.TrimSuffix(messageAuthor, "\n")}, nil
	default:
		log.Fatalf("Unknown Message type: %s, exiting...", messageType)
		return nil, UnknownMessageError
	}
}