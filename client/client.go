package client

type Client interface {
	Connect() error
	Start()
	Send(message interface{}) error
	Close()
}
