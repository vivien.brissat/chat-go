package client

import (
	"io"
	"log"
	"math/rand"
	"net"
	"saagie.io/chat-go/internal/message"
	"github.com/Pallinder/go-randomdata"
	"time"
)

type TcpClient struct {
	Name string
	connection net.Conn
	messageReader *message.Reader
	messageWriter *message.Writer
	MessagesChannel chan message.ReceivedMessage
}

func NewClient () *TcpClient {
	rand.Seed(time.Now().UnixNano())
	return &TcpClient{
		Name: randomdata.FullName(randomdata.RandomGender),
		MessagesChannel: make(chan message.ReceivedMessage),
	}
}

func (c *TcpClient) Connect() error {
	connection, err := net.Dial("tcp", "127.0.0.1:8090")
	if err != nil {
		return err
	}
	c.connection = connection
	c.messageReader = message.NewReader(connection)
	c.messageWriter = message.NewWriter(connection)

	err = c.Send(message.NameMessage{
		Name: c.Name,
	})

	if err != nil {
		log.Fatal("Impossible to set name")
	}
	return nil
}

func (c *TcpClient) Send(message interface{}) error {
	return c.messageWriter.Write(message)
}

func (c* TcpClient) Start() {
	for {
		msg, err := c.messageReader.Read()
		if err == io.EOF {
			log.Print("Network Error... Retrying")
		} else if err != nil {
			log.Fatalf("Unexpected Network Error : %v", err)
		}

		if msg != nil {
			switch typedMessage := msg.(type) {
			case message.ReceivedMessage:
				c.MessagesChannel <- typedMessage
			default:
				log.Print("Unknown message.")
			}
		}
	}
}

func (c* TcpClient) Close() error {
	return c.connection.Close()
}
