package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"saagie.io/chat-go/client"
	"saagie.io/chat-go/internal/message"
)

const (
	colorReset = "\033[0m"
	colorRed = "\033[31m"
	colorGreen = "\033[32m"
	colorYellow = "\033[33m"
	colorBlue = "\033[34m"
	colorPurple = "\033[35m"
	colorCyan = "\033[36m"
	colorWhite = "\033[37m"
)

func main() {
	client := client.NewClient()
	defer func() {
		err := client.Close()
		if err != nil {
			log.Printf("Error while trying to close client")
		}
	}()

	if err := client.Connect(); err != nil {
		log.Fatal("Impossible to connect to the client")
	}

	go client.Start()
	go func() {
		for message := range client.MessagesChannel {
			if message.ClientName != client.Name {
				fmt.Printf( "%s(%s)%s > %v \n", colorPurple, message.ClientName, colorCyan, message.Message)
			}
		}
	}()
	client.Send(message.ClientMessage{
		Message: "PWET",
	})

	scanner := bufio.NewScanner(os.Stdin)
	for {
		scanner.Scan()
		input := scanner.Text()
		if len(input) > 0 {
			client.Send(message.ClientMessage{
				Message: input,
			})
		}
	}
}
