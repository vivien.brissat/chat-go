package main

import (
	"log"
	"saagie.io/chat-go/server"
)

func main() {
	server := server.NewServer()
	defer server.Close()
	if err := server.Start(); err != nil {
		log.Fatalf("Impossible to start the server: %v", err)
	}
	server.Listen()
}