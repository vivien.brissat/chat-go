package server

import (
	"io"
	"log"
	"net"
	"saagie.io/chat-go/internal/message"
	"sync"
)

type TcpServer struct {
	listener net.Listener
	clients []*connectedClient
	mutex *sync.Mutex
}

func NewServer() *TcpServer {
	return &TcpServer{
		mutex: &sync.Mutex{},
	}
}

func (s *TcpServer) Start() error {
	listener, err := net.Listen("tcp", ":8090")
	if err != nil {
		log.Fatal("Unable create listener on port 8090")
		return err
	}
	s.listener = listener
	log.Print("Server listening on :8090")
	return nil
}

func (s* TcpServer) Listen() {
	for {
		conn, err := s.listener.Accept()
		if err != nil {
			log.Print(err)
		}
		connectedClient := s.accept(conn)
		go s.serve(connectedClient)
	}
}

func (s* TcpServer) accept(conn net.Conn) *connectedClient {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	connectedClient := &connectedClient {
		conn: conn,
		writer: message.NewWriter(conn),
	}
	s.clients = append(s.clients, connectedClient)
	log.Printf("Server accepted new connection from client %v", conn.RemoteAddr())
	return connectedClient
}

func (s* TcpServer) serve(client *connectedClient) {
	reader := message.NewReader(client.conn)
	defer s.disconnect(client)

	for {
		msg, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				log.Printf("%s deconnected. Clients connected %d", client.name, len(s.clients) - 1)
				break
			} else {
				log.Printf("Error while reading message %v", err)
			}
		}
		if msg != nil {
			switch m := msg.(type) {
			case message.ClientMessage:
				// TODO test with goroutine
				s.broadcast(message.ReceivedMessage {Message: m.Message, ClientName: client.name})
			case message.NameMessage:
				client.name = m.Name
				s.mutex.Lock()
				for indice, cli := range s.clients {
					if cli == client {
						s.clients[indice] = client
						break
					}
				}
				s.mutex.Unlock()
				log.Printf("New client %s appears", client.name)
			default:
				log.Fatalf("An unexpected message type retrieved: %v", msg)
			}
		}
	}
}

func (s* TcpServer) disconnect(client *connectedClient) {
	clientAddr := client.conn.RemoteAddr()
	// TODO use defer
	if err := client.conn.Close(); err != nil {
		log.Printf("Unabled to close connection of client: %v", clientAddr)
	}
	s.mutex.Lock()
	for indice, c := range s.clients {
		if c == client {
			s.clients = removeClient(s.clients, indice)
			break
		}
	}
	s.mutex.Unlock()
}

func removeClient(clients []*connectedClient, indice int) []*connectedClient {
	clients[indice] = clients[len(clients) - 1]
	return clients[:len(clients) - 1]
}

func (s* TcpServer) broadcast(message interface{}) {
	for _, client := range s.clients {
		err := client.writer.Write(message)
		if err != nil {
			log.Printf("Broadcasting error for client: %s", client.name)
		}
	}
}

func (s* TcpServer) Close() {
	if err := s.listener.Close() ; err != nil {
		log.Fatalf("An error occurred during close server: %v", err)
	}
}
