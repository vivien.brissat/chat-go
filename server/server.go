package server

import (
	"net"
	"saagie.io/chat-go/internal/message"
)

type connectedClient struct {
	name string
	conn net.Conn
	writer *message.Writer
}

type Server interface {
	Start() error
	Listen() //listen to client connections
	accept(conn net.Conn) *connectedClient //create the connected client
	serve(client *connectedClient) //listen to messages from the connected client
	disconnect(client *connectedClient)
	broadcast(message interface{})
	Close()
}